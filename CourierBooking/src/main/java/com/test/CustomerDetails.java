package com.test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/CustomerDetails")
public class CustomerDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{
			response.setContentType("text/html");
			
			HttpSession session = request.getSession();
			
			Connection con = Util.getConnection();
			Statement stmt = con.createStatement();
			
			String name = request.getParameter("name");
			session.setAttribute("userName", name);
			
			String contact = request.getParameter("contact");
			long ph = Long.parseLong(contact);
			String pickup = request.getParameter("pickup");
			String dest = request.getParameter("dest");
			int weight = Integer.parseInt(request.getParameter("weight"));
			session.setAttribute("pack_weight", weight);
			
			double amount = CourierDao.weightcal(weight);
			double payableAmount = CourierDao.payAmountCal(amount, pickup, dest);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Calendar cal = Calendar.getInstance();
			
			String shipment_date = dateFormat.format(cal.getTime());
			String delivery_date = CourierDao.setDeliveryDate(pickup, dest);
			
			session.setAttribute("weightCharges", amount);
			session.setAttribute("totalAmount", payableAmount);
			
			String sql = "insert into booking_details(customer_name, contact, from_pickup, to_dest, package_weight, amount, shipment_date, delivery_date) values('"+name+"', "+ph+", '"+pickup+"', '"+dest+"', "+weight+", "+payableAmount+", '"+shipment_date+"', '"+delivery_date+"' )";
			int x = stmt.executeUpdate(sql);
			
			if(x!=0)
			{
				RequestDispatcher rd = request.getRequestDispatcher("confirmBooking.jsp");
				rd.forward(request, response);
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
