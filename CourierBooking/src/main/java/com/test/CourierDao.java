package com.test;

import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CourierDao {
	
	public static int saveUser(Courier user) {
		Statement stmt = null;
		String sql = null;
		try {
			sql = "insert into booking_details(customer_name, contact, from_pickup, to_dest, package_weight, amount, shipment_date, delivery_date)"
					+ " values('" + user.getCustomerName()
					+ "', '" + user.getContact()
					+ "', '" + user.getFrom_pickup()
					+ "', '" + user.getTo_dest()
					+ "', '" + user.getWeight()
					+ "', " + user.getAmount()
					+ ", '" + user.getShipmet_date()
					+ "', '"+user.getDelivery_date()
					+"');";
			stmt = Util.getConnection().createStatement();
			return stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
public static double weightcal(int weight) {
		
		double amt = 50.00;//basic minimum charges
	
		if(weight < 250)
        {
            amt += weight * 0.20;
        }
        else if(weight < 500)
        {
            amt += 250 * 0.2 + (weight - 250) * 0.4;
        }
        else if(weight < 1000)
        {
            amt += 250 * 0.2 + 500 * 0.4 + (weight - 500) * 0.7;
        }	
        else if(weight < 1500) 
        {	
        	amt += 250 * 0.2 + 500 * 0.4 + 1000 * 0.7 +(weight - 1000) * 1;	
        }
        else if(weight > 1500) 
        {	
        	amt += 250 * 0.2 + 500 * 0.4 + 1000 * 0.7 + 1500 * 1 +(weight - 1500) * 2;	
        }
	
		//System.out.println(amt);	
		return amt;
	}

public static double payAmountCal(double weightamt, String from_pickup, String to_dest)
{
	double courier_amt = 0.00;

	if ((from_pickup.equals("Bangalore") || from_pickup.equals("Mangalore")) && (to_dest.equals("Bangalore") || to_dest.equals("Mangalore"))){
		  courier_amt= weightamt + 100.00 ;
	}
	
	else if ((from_pickup.equals("Bangalore") || from_pickup.equals("Hyderabad")) && (to_dest.equals("Bangalore") || to_dest.equals("Hyderabad"))){
		courier_amt=(weightamt + 400.00) ;
	}
	
	else if ((from_pickup.equals("Mangalore") || from_pickup.equals("Hyderabad")) && (to_dest.equals("Mangalore") || to_dest.equals("Hyderabad"))){
		courier_amt=(weightamt + 600.00) ;
	}
	
	return courier_amt;
	
}

public static String setDeliveryDate(String from_pickup, String to_dest)
{
	String delivery_date = null;
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Calendar cal = Calendar.getInstance();
	
	if ((from_pickup.equals("Bangalore") || from_pickup.equals("Mangalore")) && (to_dest.equals("Bangalore") || to_dest.equals("Mangalore"))){

      cal.add(Calendar.DATE, 3);
      delivery_date = dateFormat.format(cal.getTime());
	}
	
	else if ((from_pickup.equals("Bangalore") || from_pickup.equals("Hyderabad")) && (to_dest.equals("Bangalore") || to_dest.equals("Hyderabad"))){

	    cal.add(Calendar.DATE, 5);
	    delivery_date = dateFormat.format(cal.getTime());
	}
	
	else if ((from_pickup.equals("Mangalore") || from_pickup.equals("Hyderabad")) && (to_dest.equals("Mangalore") || to_dest.equals("Hyderabad"))){

		cal.add(Calendar.DATE, 7);
		  delivery_date = dateFormat.format(cal.getTime());
	}
	
	return delivery_date;
	
}
	
}
