package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CustomerDetailsTest {
	Courier c = new Courier();
	@Test
	public void details() throws Exception {
		
		c.setCustomerName("Appu");
		c.setContact(7214325181L);
		c.setFrom_pickup("Hyderabad");
		c.setTo_dest("Bangalore");
		c.setWeight(1000);
		c.setAmount(1050);
		c.setShipmet_date("27/09/2022");
		c.setDelivery_date("05/12/2022");
		c.setTracking_no(10);
		
		assertEquals(1, CourierDao.saveUser(c) );
	}

}
