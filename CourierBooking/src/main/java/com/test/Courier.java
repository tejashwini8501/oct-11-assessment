package com.test;

public class Courier {
	
	String customerName;
	long contact;
	String from_pickup;
	String to_dest;
	int weight;
	double amount;
	String shipmet_date;
	String delivery_date;
	int tracking_no;
	
	public Courier() {
		super();
	}
	
	public Courier(String customerName, long contact, String from_pickup, String to_dest, int weight, double amount, String shipment_date,
			String delivery_date, int tracking_no) {
		super();
		this.customerName = customerName;
		this.contact = contact;
		this.from_pickup = from_pickup;
		this.to_dest = to_dest;
		this.weight = weight;
		this.amount = amount;
		this.shipmet_date = shipment_date;
		this.delivery_date = delivery_date;
		this.tracking_no = tracking_no;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getContact() {
		return contact;
	}

	public void setContact(long contact) {
		this.contact = contact;
	}

	public String getFrom_pickup() {
		return from_pickup;
	}

	public void setFrom_pickup(String from_pickup) {
		this.from_pickup = from_pickup;
	}

	public String getTo_dest() {
		return to_dest;
	}

	public void setTo_dest(String to_dest) {
		this.to_dest = to_dest;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getShipmet_date() {
		return shipmet_date;
	}

	public void setShipmet_date(String shipmet_date) {
		this.shipmet_date = shipmet_date;
	}

	public String getDelivery_date() {
		return delivery_date;
	}

	public void setDelivery_date(String delivery_date) {
		this.delivery_date = delivery_date;
	}

	public int getTracking_no() {
		return tracking_no;
	}

	public void setTracking_no(int tracking_no) {
		this.tracking_no = tracking_no;
	}
	
	
}
