<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.test.Util"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<style type="text/css">
	table {
		margin-top: 5%;
		width: 40%;
		margin-left: auto;
		margin-right: auto;
	}
	
	table tr td {
		text-align: left;
		padding: 10px 10px 10px 100px;
		font-size : 20px;
	}
</style>
</head>
<body>

	<div class="booking-form">
	<div class="form_settings">
	<h1>Booking details</h1>

	<%
		try
		{
			session = request.getSession();
			
			Connection con = Util.getConnection();
			Statement stmt = con.createStatement();
			
			String userName  = (String) session.getAttribute("userName");
			
			String sql = "select * from booking_details where customer_name = '"+userName+"'";
			ResultSet rs = stmt.executeQuery(sql);
			
			%>
			<table>
			
			<%
			
			if(!rs.next()) {
			
			%>
				<tr>
						<td colspan=4 align="center">You have not made any bookings yet!</td>
				</tr>
				
			<%
			}
			
			else
			{
			
			%>	
				<tr>
					<td >Name</td>
					<td><%= rs.getString("customer_name") %></td>
				</tr>
				<tr>
					<td>Contact No</td>
					<td><%= rs.getLong("contact") %></td>
				</tr>
				<tr>
					<td>Pick up</td>
					<td><%= rs.getString("from_pickup") %></td>
				</tr>
				<tr>
					<td>Delivery place</td>
					<td><%= rs.getString("to_dest") %></td>
				</tr>
				<tr>
					<td>Shipping date</td>
					<td><%= rs.getString("shipment_date") %></td>
				</tr>
				<tr>
					<td>Delivery date</td>
					<td><%= rs.getString("delivery_date") %></td>
				</tr>
				<tr>
					<td>Amount paid</td>
					<td><%= rs.getDouble("amount") %></td>
				</tr>
				<tr>
					<td>Tracking No</td>
					<td><%= rs.getInt("tracking_no") %></td>
				</tr>
		<%
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		%>
		</table>
		
	</div>
	</div>

</body>
</html>